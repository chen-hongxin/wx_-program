    |-- pages
    |   |-- auth（授权页面）
    |   |-- cart（购物车页面）
    |   |-- category（分类页面）
    |   |-- collect（收藏页面）
    |   |-- feedback（意见反馈页面）
    |   |-- goods_detail（商品详情页面）
    |   |-- goods_list（商品列表页面）
    |   |-- index（首页）
    |   |-- login（登陆页面）
    |   |-- order（订单页面）
    |   |-- pay（结算页面）
    |   |-- search（搜索页面）
    |   |-- user（个人中心）
    |-- components（存放组件）
    |-- icons（存放图标）
    |-- lib（存放第三方库）
    |-- request（自写的接口帮助库）
    |-- styles（存放公共样式）
    |-- utils（自写的帮助库）
    |-- typings（微信小程序的 typescript 声明文件）

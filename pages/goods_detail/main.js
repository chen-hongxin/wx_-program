/*
  1 发送请求获取数据
  2 点击轮播图 预览大图
    ① 给轮播图绑定点击事件
    ② 调用小程序的api previewImage
  3 点击加入购物车
    ① 先绑定点击事件
    ② 获取缓存中的购物车数据（数组格式）
    ③ 先判断当前的商品是否存在于购物车
      ① 若已经存在 则执行商品数量++
      ② 若不存在 则直接给购物车数组添加新元素 带上购买数量属性num 重新把购物车数组填充回缓存
    ④ 弹出提示
  4 商品收藏
    ① 页面 onShow 的时候，加载缓存中的商品收藏的数据
    ② 判断当前商品是否被收藏（若是则点亮页面的图标）
    ③ 点击商品收藏按钮
      判断该商品是否存在于缓存数组中
        壹 若已经存在，则把该商品删除
        贰 若不存在，则把商品添加到收藏数组中，并存入到缓存中

*/

import { request } from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
// pages/goods_detail/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsObj: {},
    // 商品是否被收藏
    isCollect: false
  },
  // 商品对象
  GoodsInfo: {},

  /**
   * 生命周期函数--监听页面加载
   */
  onShow: function () {
    let pages = getCurrentPages();
    let currentPage = pages[pages.length - 1];
    let options = currentPage.options;
    const {goods_id} = options;
    this.getGoodsDetail(goods_id);
  },

  // 获取商品详情数据
  async getGoodsDetail(goods_id) {
    const goodsObj = await request({url:"/goods/detail", data:{goods_id}});
    this.GoodsInfo = goodsObj;
    // 1 获取缓存中的商品收藏的数组
    let collect = wx.getStorageSync('collect') || [];
    // 2 判断当前的商品是否被收藏
    let isCollect = collect.some(v => v.goods_id === this.GoodsInfo.goods_id);

    this.setData({
      goodsObj: {
        goods_name: goodsObj.goods_name,
        goods_price: goodsObj.goods_price,
        /*
        iphone部分手机不识别 webp图片格式
          方法一：让后台进行修改
          方法二（临时自己改）：确保后台存在对应的jpg图片格式 对webp图片格式进行替换
            goods_introduce: goodsObj.goods_introduce.replace(/\.webp/g, '.jpg'),
        */
        goods_introduce: goodsObj.goods_introduce,
        pics: goodsObj.pics
      },
      isCollect
    })
  },


  //点击轮播图 放大预览
  handlePrevewImage(e) {
    // console.log(this.data);
    // 1 先构造要预览的图片数组
    const urls = this.GoodsInfo.pics.map(v => v.pics_mid);
    // 2 接收传递过来的图片url
    const current = e.currentTarget.dataset.url;
    wx.previewImage({
      current,
      urls
    })
  },

  // 点击“加入购物车”
  handleCartAdd() {
    // 1 获取缓存中的购物车数组
    let cart = wx.getStorageSync('cart') || [];
    // 2 判断商品对象是否存在于购物车数组中
    let index = cart.findIndex(v => v.goods_id === this.GoodsInfo.goods_id);
    if (index === -1) {
      // 3 不存在 即第一次添加
      this.GoodsInfo.num = 1;
      this.GoodsInfo.checked = true;
      cart.push(this.GoodsInfo);
    } else {
      // 4 购物车中已经存在该数据 执行num++
      cart[index].num++;
    }
    // 5 把购物车重新添加回缓存中
    wx.setStorageSync('cart', cart);
    // 6 弹窗提示
    wx.showToast({
      title: '加入成功',
      icon: 'success',
      // true 节流阀的作用
      mask: true
    });
      
  },

  // 点击“商品收藏图标”
  handleCollect() {
    let isCollect = false;
    // 1 获取缓存中的商品收藏数组
    let collect = wx.getStorageSync('collect') || [];
    // 2 判断该商品是否被收藏过
    let index = collect.findIndex(v => v.goods_id === this.GoodsInfo.goods_id);
    // 3 当 index 不等于-1，则表示已经收藏过该商品
    if (index !== -1) {
      // 在数组中删除该商品（已经收藏过，点击为取消收藏）
      collect.splice(index, 1);
      isCollect = false;
      wx.showToast({
        title: '取消成功',
        icon: 'success',
        mask: true
      });
    } else {
      // 添加该商品到数组中（未收藏过，点击为收藏）
      collect.push(this.GoodsInfo);
      isCollect = true;
      wx.showToast({
        title: '收藏成功',
        icon: 'success',
        mask: true
      });
    }
    // 4 把数组存入到缓存中
    wx.setStorageSync('collect', collect);
    // 5 修改data中的属性 isCollect
    this.setData({
      isCollect
    })
  }
})
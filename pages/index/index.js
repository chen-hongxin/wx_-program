// 0. 引入用来发送请求的方法（要把路径补全）
import { request } from "../../request/index.js"

Page({
  data: {
    // 轮播图数组
    swiperList: [],
    // 导航数组
    navList: [],
    // 楼层数据
    floorList: []
  },

  // 页面开始加载就会触发
  onLoad: function (options) {
    // 1.发送异步请求请求获取轮播图数据 优化的手段可以通过es6的 promise来解决这个问题
    // wx.request({
    //   url: 'https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata',
    //   success: res => {
    //     this.setData({
    //       swiperList: res.data.message
    //     })
    //   }
    // })

    this.getSwiperList();
    this.getnavList();
    this.getFloorList();
  },

  // 获取轮播图数据
  getSwiperList() {
    request({url: '/home/swiperdata'})
    .then(result => {
      this.setData({
        swiperList: result
      })
    })
  }, 
  // 获取分类导航数据 
  getnavList() {
    request({url: '/home/catitems'})
    .then(result => {
      this.setData({
        navList: result
      })
    })
  },
  getFloorList() {
    request({url: '/home/floordata'})
    .then(result => {
      result.forEach((v, i) => {
        v.product_list.forEach((v, i) => {     
          let p = v.navigator_url.split("?")
          p[0] = p[0] + "/index";
          v.navigator_url = p.join("?")
        })
      })
      this.setData({
        floorList: result
      })
    })
  }
})
/*
1 用户上滑页面 滚动条触底 开始加载下一页数据
  1 找到滚动条触底事件（微信小程序官方开发文档中寻找）
  2 判断还有没有下一页数据
    1 获取到总页数
      总页数 = Math.ceil(总条数 / 页容量[pagesize])
            = Math.ceil( 23 / 10 ) = 3
    2 获取到当前的页码 pagenum
    3 判断一下 当前的页码是否大于等于总页数
  3 假如没有下一页数据 弹出一个提示
  4 假如还有下一页数据 来加载下一页数据
    1 当前的页码+1
    2 重新发送数据请求
    3 将请求到的数据与data中的原数据进行拼接后 赋值给data
2 下拉刷新页面
  1 触发下拉刷新事件(需要在页面的 json文件中开启一个配置项)
    找到触发下拉刷新的事件
  2 重置 数据 数组
  3 重置页码 设置为1
  4 重新发送请求
  5 数据请求回来后需要手动的关闭等待效果
*/
import { request } from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';
// pages/goods_list/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      {
        id: 0,
        value: "综合",
        isActive: true
      },{
        id: 1,
        value: "销量",
        isActice: false
      },{
        id: 2,
        value: "价格",
        isActive: false
      }
    ],
    goods_list: []
  },
  // 接口要的参数
  QueryParams: {
    query: '',
    cid: '',
    pagenum: 1,
    pagesize: 10
  },
  // 总页数
  totalPages: 1,

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.QueryParams.cid = options.cid || "";
    this.QueryParams.query = options.query || "";
    this.getGoodsList();
  },

  // 获取商品列表数据
  async getGoodsList() {
    const res = await request({url:'/goods/search', data:this.QueryParams});
    // 获取总条数
    const total = res.total;
    // 获取总页数
    this.totalPages = Math.ceil(total/ this.QueryParams.pagesize);
    // console.log(this.totalPages);
    this.setData({
      // 拼接了数据
      goods_list: [...this.data.goods_list, ...res.goods]
    })
    // 关闭下拉刷新的窗口(如果没有调用下拉刷新的窗口,直接关闭也不会报错)
    wx.stopPullDownRefresh();
  },

  // 标题点击事件（从子组件传过来的）
  handletabsItemChange(e) {
    // console.log(e);
    // 1 获取被点击的标题索引
    const {index} = e.detail;
    // 2 修改源数组
    let {tabs} = this.data;
    tabs.forEach((v, i) =>
     i === index ? v.isActive = true : v.isActive = false);
     // 3 赋值到data中
     this.setData({
       tabs
     })
  },

  // 页面上滑 滚动条触底事件
  onReachBottom() {
    // 1 判断还有没有下一页数据
      if(this.QueryParams.pagenum >= this.totalPages) {
        // 没有下一页数据
        // console.log('没有下一页数据');
        wx.showToast({
          title: '没有下一页数据了',
        })
      } else {
        // 还有下一页数据
        this.QueryParams.pagenum++;
        this.getGoodsList();
      }
  },

  // 下拉刷新事件
  onPullDownRefresh() {
    // 1 重置数组
    this.setData({
      goods_list: []
    })
    // 2 重置页码
    this.QueryParams.pagenum = 1;
    // 3 发送请求
    this.getGoodsList();
  }
})
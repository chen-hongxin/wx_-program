/*
  1 页面加载的时候 
    ① 从缓存中获取购物车数据，将其中 checked属性为 true 的数据渲染到页面中
  2 微信支付（哪些人、哪些账号可以实现微信支付）
    ① 企业账号
    ② 企业账号的小程序后台中，必须给开发者添加上白名单 
      一个 appid 可以同时绑定多个开发者，这些开发者可以公用这个 appid 和他的开发权限
*/


import { getSetting, chooseAddress, openSetting, showModal, showToast} from "../../utils/asyncWx.js"
import regeneratorRuntime from '../../lib/runtime/runtime';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    address: {},
    cart: [],
    totalPrice: 0,
    totalNum: 0
  },

  onShow() {
    // 1.1 获取缓存中的收货地址信息
    const address = wx.getStorageSync('address');
    // 1.2 获取缓存中的购物车数据
    let cart = wx.getStorageSync('cart') || [];
    // 过滤后的购物车数组(每一个元素的 checked属性为true)
    cart = cart.filter(v => v.checked);
    this.setData({address});

    // 计算总价格和总数量
    let totalPrice = 0;
    let totalNum = 0;
    cart.forEach(v => {
        totalPrice += v.num * v.goods_price;
        totalNum += v.num;
    })

    // 把更新后的购物车数据返回 data 和缓存中
    this.setData({
      cart,
      totalPrice,
      totalNum,
      address
    });
    wx.setStorageSync('cart', cart);
  },

})
/*
  1 输入框绑定事件（输入框的值改变就会触发）
    ① 获取到输入框的值
    ② 合法性判断
    ③ 检验通过，把输入框的值发送到后台并进行信息匹配
    ④ 返回的数据打印到页面上
  2 防抖技术（实现的关键是一个定时器）
    ① 定义全局的定时器id
  3 防抖和节流
    ① “防抖”一般用在输入框中，防止重复输入、重复发送请求
    ② “节流”一般用在页面下拉和上拉过程中
*/

import { request } from "../../request/index.js";
import regeneratorRuntime from '../../lib/runtime/runtime';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    goods: [],
    // “取消”按钮是否显示
    isShow: false,
    // 输入框的值
    inputValue: ''
  },

  TimeId: -1,

  // 输入框的值改变了就会触发的事件
  handleInput(e) {
    // console.log(e);
    // 1 获取输入框的值
    const {value} = e.detail;
    // 2 检测合法性
    if(!value.trim()) {
      // 隐藏“取消”按钮与清空goods数组
      this.setData({
        goods: [],
        isShow: false
      });
      // 值不合法
      return;
    }
    // 让“取消”按钮显示
    this.setData({
      isShow: true
    })
    // 3 准备发送请求获取数据（此处用了防抖技术）
    clearTimeout(this.TimeId);
    this.TimeId = setTimeout(() => {
      this.qsearch(value);
    }, 1000);
  },

  // 发送请求获取“搜索数据”
  async qsearch(query) {
    const res = await request({url:'/goods/qsearch', data:{query}});
    // console.log(res);
    this.setData({
      goods: res
    })
  },

  // 点击“取消”按钮
  handleCancel() {
    this.setData({
      inputValue: '',
      isShow: false,
      goods: []
    })
  }
})
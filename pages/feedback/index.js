/* 
  1 点击 “+” 触发 tap 点击事件
    1 调用小程序内置的选择图片的 api
    2 获取存储图片路径的数组
    3 把图片路径存储到 data 的变量中
    4 页面根据图片数组进行循环显示
  2 点击 自定义图片组件
    1 获取被点击的元素的索引
    2 获取 data 中的图片数组
    3 根据索引 在数组中删除对应的元素
    4 把数组重新存储 data 中
  3 点击“提交”按钮
    1 获取文本域的内容（类似于获取输入框内容）
      ① data中定义一个变量用来表示文本框内容
      ② 文本域绑定输入事件，事件触发时，把输入框的值存入到变量中
    2 对内容进行合法性验证（验证不通过则弹出提示信息）
    3 验证通过，将用户选择的图片上传到专门处理图片的服务器中
      服务器会返回图片外网的链接
        ① 遍历图片数组
        ② 挨个上传
        ③ 维护新图片数组（图片上传后返回的外网链接）
    4 将文本与内容和图片路径的数据一起提交到服务器
    5 清空当前页面
    6 返回上一页
*/
Page({
  data: {
    tabs: [
      {
        id: 0,
        value: "体验问题",
        isActice: true
      },
      {
        id: 1,
        value: "商家、商品投诉",
        isActice: false
      }
    ],
    // 存储被选中的图片路径的数组
    chooseImgs: [],
    // 文本域的内容
    textVal: ""
  },

  // 存储外网图片路径的数组
  UpLoadImgs: [],

  // 标题点击事件（从子组件传过来的）
  handleTabsItemChange(e) {
    // 1 获取被点击的标题索引
    const {index} = e.detail;
    // 2 修改源数组
    let {tabs} = this.data;
    tabs.forEach((v, i) =>
    i === index ? v.isActive = true : v.isActive = false);
    // 3 赋值到data中
    this.setData({
      tabs
    })
  },

  // 点击 “+” 选择图片
  handleChooseImg() {
    // 调用小程序内置的选择图片api
    wx.chooseImage({
      // 最多可以选择的图片张数，默认9
      count: 9, 
      // original 原图，compressed 压缩图，默认二者都有
      sizeType: ['original', 'compressed'], 
      // album 从相册选图，camera 使用相机，默认二者都有
      sourceType: ['album', 'camera'], 
      success: res => {
        // console.log(res);
        this.setData({
          // 对后面添加的图片与原数组进行拼接
          chooseImgs: [...this.data.chooseImgs, ...res.tempFilePaths]
        })
      }
    })
  },

  // 点击 自定义图片组件
  handleRemoveImg(e) {
    // 获取被点击的组件的索引
    const {index} = e.currentTarget.dataset;
    // 获取 data 中的图片数组
    let {chooseImgs} = this.data;
    // 删除元素
    chooseImgs.splice(index, 1);
    this.setData({
      chooseImgs
    })
  },

  // 文本框输入事件
  handleTextInput(e) {
    this.setData({
      textVal: e.detail.value
    })
  },

  // 提交按钮的点击
  handleFormSubmit() {
    // 1 获取文本域的内容
    const {textVal, chooseImgs} = this.data;
    // 2 合法性验证
    if (!textVal.trim()) {
      wx.showToast({
        title: '输入不合法',
        icon: 'none',
        mask: true
      });
      return;
    }
    // 3 准备上传图片到专门的图片服务器

    // 显示“等待”弹窗
    wx.showLoading({
      title: '正在上传中',
      mask: true
    })

    // 判断有没有需要上传的图片数组
    if (chooseImgs.length != 0) {
      // 上传文件的 api 不支持多个文件同时上传
      // 故此处采用遍历数组 挨个上传的办法
      chooseImgs.forEach((v, i) => {
        wx.uploadFile({
          filePath: v,
          name: 'file',
          url: '',
          success: (res) => {
            // console.log(res);
            let url = JSON.parse(result.data).url;
            this.UpLoadImgs.push(url);

            // 所有图片上传完毕触发
            if (i === chooseImgs.length - 1) {
              // 关闭“等待”弹窗
              wx.hideLoading();
              // 提交成功
              console.log("把文本的内容和外网的图片数组提交到后台中");
              // 重置页面
              this.setData({
                textVal: "",
                chooseImgs: []
              })
              // 返回上一个页面
              wx.navigateBack({
                delta: 1, // 回退前 delta(默认为1) 页面
              });
            }
          }
        })
      })
    } else {
      wx.hideLoading({});
      wx.navigateBack({
        delta: 1, // 回退前 delta(默认为1) 页面
      });
    }
  }
}) 
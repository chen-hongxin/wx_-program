# 红心优购

## 介绍
一个购物商城微信小程序项目，各功能较为完善

## 使用说明
直接使用微信开发者工具运行即可

## 接口文档
[https://www.showdoc.com.cn/128719739414963/2513282464078443](https://www.showdoc.com.cn/128719739414963/2513282464078443)

## 帮助文档
- [小程序开发文档](https://developers.weixin.qq.com/miniprogram/dev/framework/)
- [阿⾥巴巴字体 iconfont](https://www.iconfont.cn/)

## 项目搭建

### 目录结构
    |-- pages
    |-- components（存放组件）
    |-- icons（存放图标【可从阿里巴巴字体图标网站引入】）
    |-- lib（存放第三方库）
    |-- request（自写的接口帮助库）
    |-- styles（存放公共样式）
    |-- utils（自写的帮助库）
    |-- typings（微信小程序的 typescript 声明文件）

### 项目页面
    |-- pages
    |   |-- auth（授权页面）
    |   |-- cart（购物车页面）
    |   |-- category（分类页面）
    |   |-- collect（收藏页面）
    |   |-- feedback（意见反馈页面）
    |   |-- goods_detail（商品详情页面）
    |   |-- goods_list（商品列表页面）
    |   |-- index（首页）
    |   |-- login（登陆页面）
    |   |-- order（订单页面）
    |   |-- pay（结算页面）
    |   |-- search（搜索页面）
    |   |-- user（个人中心）

### 搭建 tabBar 结构（app.json 文件中）

```
"tabBar": {
    "color": "#999",
    "selectedColor": "#ff2d4a",
    "backgroundColor": "#fafafa",
    "position": "bottom",
    "borderStyle": "black",
    "list": [
      {
        "pagePath": "pages/index/index",
        "text": "首页",
        "iconPath": "icons/home.png",
        "selectedIconPath": "icons/home-o.png"
      }, {
        "pagePath": "pages/category/index",
        "text": "分类",
        "iconPath": "icons/category.png",
        "selectedIconPath": "icons/category-o.png"
      }, {
        "pagePath": "pages/cart/index",
        "text": "购物车",
        "iconPath": "icons/cart.png",
        "selectedIconPath": "icons/cart-o.png"
      }, {
        "pagePath": "pages/user/index",
        "text": "我的",
        "iconPath": "icons/my.png",
        "selectedIconPath": "icons/my-o.png"
      }
    ]
  },
```

## 项目开发细节

### 1. 首页
#### 1.1 效果图
![输入图片说明](https://images.gitee.com/uploads/images/2021/1027/220757_6eb8a2b7_8035370.jpeg "1.jpg")
#### 1.2 业务逻辑
- tabBar 实现底部导航栏
- 自定义组件 SearchInput 实现头部搜索框
- 加载“轮播图”数据（后面所跟的是获取数据所使用的接口） => [https://api.zbztb.cn/api/public/v1/home/swiperdata](https://api.zbztb.cn/api/public/v1/home/swiperdata)
- 加载“导航”数据 => [https://api-hmugo-web.itheima.net/api/public/v1/home/catitems](https://api-hmugo-web.itheima.net/api/public/v1/home/catitems)
- 加载“楼层”数据 => [https://api.zbztb.cn/api/public/v1/home/floordata](https://api.zbztb.cn/api/public/v1/home/floordata)
#### 1.3 关键技术
- 小程序内置  **request API** 
- ES6 的 `Promise` 函数
- 小程序内置组件  **swiper**  
- 自定义组件  **SearchInput** 

### 2. 分类页面
#### 2.1 效果图
![分类页面](https://images.gitee.com/uploads/images/2021/1027/220955_609dcdbd_8035370.jpeg "2.jpg")
#### 2.2 业务逻辑
- 初始状态，右侧页面加载左侧菜单首标签对应的数据
- 点击左侧菜单的不同标签，右侧页面动态渲染不同的数据
- “分类页面”数据 => [https://api-hmugo-web.itheima.net/api/public/v1/categories](https://api-hmugo-web.itheima.net/api/public/v1/categories)
#### 2.3 关键技术
- 小程序内置组件  **scroll-view**  
-  **ES7**  的 `async` 和 `await`（ **ES7**  的 `async` 号称解决回调的最终方案）
#### 2.4 小程序中使用` async` 语法
- 微信开发者工具右上角的详情 -> 本地设置 -> 将 JS 编译成 ES5(勾选)
- 在小程序目录下新建文件 `lib/runtime/runtime.js` 
- 将 **Facebook**  的  **regenerator**  库中，以下文件中的代码拷贝进上一步新建的文件中 [regenerator/packages/regenerator-runtime/runtime.js](https://github.com/facebook/regenerator/blob/5703a79746fffc152600fdcef46ba9230671025a/packages/regenerator-runtime/runtime.js)
- 在每一个需要使用 `async` 语法的 `.js` 文件中引入：<br>
&emsp;&emsp;`import regeneratorRuntime from '../../lib/runtime/runtime';`<br>
注意：不能全局引入

### 3. 商品列表页面
#### 3.1 效果图
![商品列表页面](https://images.gitee.com/uploads/images/2021/1027/221008_51758585_8035370.jpeg "3.jpg")
#### 3.2 业务逻辑
- 加载“商品列表”数据 => [https://api.zbztb.cn/api/public/v1/goods/search](https://api.zbztb.cn/api/public/v1/goods/search)
- 启用页面下拉刷新功能
    - 页面的  **json**  文件中设置 `enablePullDownRefresh:true `
    - 页面的  **js**  文件中，绑定事件 `onPullDownRefresh`
- 启用页面上拉触底“加载更多”功能
    - 页面的  **js**  文件中，绑定事件 `onReachBottom`
    - 加载下一页数据
#### 3.3 关键技术
- 小程序  **json**  配置文件中启用  **上拉加载**  和  **下拉加载**  功能
- 自定义组件  **SearchInput** 和  **Tabs** 

### 4. 商品详情页面
#### 4.1 效果图
![商品详情页面](https://images.gitee.com/uploads/images/2021/1027/221019_48d22e9e_8035370.jpeg "4.jpg")
#### 4.2 业务逻辑
- 渲染商品详情数据 => [https://api.zbztb.cn/api/public/v1/goods/detail](https://api.zbztb.cn/api/public/v1/goods/detail)
- 实现全屏预览图片功能
- 点击“收藏”按钮，收藏商品
- 联系客服
- 分享
- 加入购物车（使用本地存储来维护购物车数据）
- 立即购买（使用到 [创建订单接口](https://api-hmugo-web.itheima.net/api/public/v1/my/orders/create)）
#### 4.3 关键技术
-  **swiper**  组件
-  “图片预览功能”使用了微信小程序内置 **API** [wx.previewImage(Object object)](https://developers.weixin.qq.com/miniprogram/dev/api/media/image/wx.previewImage.html)
- 本地存储实现“收藏”功能
- “联系客服”与“分享功能”使用了 `button` 按钮的内置函数
- “图文详情”部分使用了 **富文本标签** 渲染 **富文本** 

### 5. 商品收藏页面
#### 5.1 效果图
![商品收藏页面](https://images.gitee.com/uploads/images/2021/1027/221038_13d9898e_8035370.jpeg "5.jpg")
#### 5.2 业务逻辑
- 获取本地存储中相应的数据进行渲染
- 点击收藏页中的不同商品可以跳转到对应的商品详情页面
#### 5.3 关键技术
- 从本地存储中加载数据

### 6. 购物车页面
#### 6.1 效果图
![购物车页面](https://images.gitee.com/uploads/images/2021/1027/221051_7c81386a_8035370.jpeg "6-(2).jpg")
#### 6.2 业务逻辑
- 渲染购物车中的商品数据（数据从本地存储获得）
- 添加收货地址
- 修改商品数量
- 单选和全选功能
- 结算功能（跳转到支付页面）
#### 6.3 关键技术
- 从本地存储中加载数据
- “添加收货地址”功能使用了微信小程序内置 **API** [wx.chooseAddress(Object object)](https://developers.weixin.qq.com/miniprogram/dev/api/open-api/address/wx.chooseAddress.html)
- 小程序 **复选框** 组件

### 7. 支付页面
#### 7.1 效果图
![支付页面](https://images.gitee.com/uploads/images/2021/1027/221130_c5efdbce_8035370.jpeg "7.jpg")
#### 7.2 业务逻辑
- 获取收货地址
- 渲染购物车中要结算的商品数据
- 支付功能（暂未实现）
    - ① 获取微信的登录信息
    - ② 获取后台返回的支付相关参数
    - ③ 调用微信接口实现 **支付** 
    - ④ 支付成功创建订单
    - ⑤ 跳转到订单页面
#### 7.3 支付流程
7.3.1 示意图<br>
![支付流程](https://images.gitee.com/uploads/images/2021/0923/162433_766c39d5_8035370.jpeg "6.jpg")<br>
7.3.2 有关接口
- ① 获取预支付参数 => https://api.zbztb.cn/api/public/v1/my/orders/req_unifiedorder
- ② 创建订单 => https://api.zbztb.cn/api/public/v1/my/orders/create
- ③ 更新订单状态 => https://api.zbztb.cn/api/public/v1/my/orders/chkOrder
#### 7.3 关键技术
- 小程序 [支付 API](http://https://developers.weixin.qq.com/miniprogram/dev/wxcloud/guide/wechatpay/wechatpay.html)

### 8. 授权页面（暂未实现）
#### 8.1 效果图
无
#### 8.2 业务逻辑
![逻辑图](https://images.gitee.com/uploads/images/2021/0923/152257_12b3e253_8035370.jpeg "1632381675(1).jpg")<br>
8.2.1 获取用户信息
- 返回 `encryptedData、rawData、iv、signature`

8.2.2 小程序登录
- 返回 `code`

8.2.3 执行 **post** 请求，提交数据到后台
- 提交数据 `encryptedData、rawData、iv、signature` 到后台，返回 **token** 
- 所用的接口为 https://api.zbztb.cn/api/public/v1/users/wxlogin

### 9. 订单列表页面（暂未实现）
#### 9.1 效果图
无
#### 9.2 业务逻辑
- 根据不同的状态去加载不同的订单数据 => https://api.zbztb.cn/api/public/v1/my/orders/all

#### 9.3 关键技术
- ⾃定义组件，⽗向⼦动态传参 `this.selectComponent("#tabs")`
- **时间戳** 格式化处理

### 10. 搜索中心页面
#### 10.1 效果图
![搜索中心页面](https://images.gitee.com/uploads/images/2021/1027/221424_66469665_8035370.jpeg "1.jpg")
#### 10.2 业务逻辑
- 获取输入框的值返回给后台进行搜索 => https://api.zbztb.cn/api/public/v1/goods/qsearch
- 将后台返回的数据渲染到页面上
- 点击“取消”按钮时，清除输入状态
#### 10.3 关键技术
- 防抖技术

## 11. 个人中心页面
#### 11.1 效果图
![个人中心页面](https://images.gitee.com/uploads/images/2021/1027/221432_f5055369_8035370.jpeg "2.jpg")
#### 11.2 业务逻辑
- 获取用户登录信息
- 加载商品收藏信息（数据从本地存储中获取）
- 查询订单信息（未实现） => https://api.zbztb.cn/api/public/v1/my/orders/all
#### 11.3 关键技术
- 获取用户登录信息使用了小程序内置 **API** [wx.getUserInfo(Object object)](https://developers.weixin.qq.com/miniprogram/dev/api/open-api/user-info/wx.getUserInfo.html)
- [CSS3 filter(滤镜) 属性](https://www.runoob.com/cssref/css3-pr-filter.html) 

### 12. 意见反馈页面
#### 12.1 效果图
![意见反馈页面](https://images.gitee.com/uploads/images/2021/0923/162826_d11b9e74_8035370.jpeg "1632385668(1).jpg")
#### 12.2 业务逻辑
- 点击 **“按钮 +”** 可以选择本地图片，显示到页面上
- 点击图片，会移除
#### 12.3 关键技术
- 自定义组件 **Tabs** 
- 小程序内置的选择图片  **API**  [wx.chooseImage(Object object)](https://developers.weixin.qq.com/miniprogram/dev/api/media/image/wx.chooseImage.html)


